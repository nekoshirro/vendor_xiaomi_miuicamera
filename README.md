# MiuiCamera 

## Getting Started

### Note : This MiuiCamera is non-modified edition & also you may need required patches in your device side.

#### Cloning :
• Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/nekoshirro/vendor_xiaomi_miuicamera -b thirteen vendor/xiaomi/miuicamera
```
• To build, Inherit the config by adding this in your device's makefile :
```
$(call inherit-product-if-exists, vendor/xiaomi/miuicamera/config.mk)
```
• To ship MiuiGallery :
```
export TARGET_SHIPS_GALLERY=true
```
or add this line in your device's makefile :
```
TARGET_SHIPS_GALLERY := true
```
